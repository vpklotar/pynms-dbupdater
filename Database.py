import time
import _thread
import GlobalConfig


class Database:

    def __init__(self, config):
        self.Config = config
        self.Commands = []
        self.Logger = GlobalConfig.GetLogger(self.__class__.__name__)
        if "Sleep" in config:
            self.Sleep = float(config['Sleep'])
        else:
            self.Sleep = 0.1
        self.Logger.debug("Using sleep value of %f" % self.Sleep)
        self.StartProcessingCommands()

    def Update(self, data):
        self.Logger.error("Update function not implemented")

    def Delete(self, data):
        self.Logger.error("Delete function not implemented")

    def Insert(self, data):
        self.Logger.error("Insert function not implemented")

    def AddCommand(self, command):
        self.Commands.append(command)

    def StartProcessingCommands(self):
        self.Logger.debug("Starting new processing thread")
        self.ProcessThread = _thread.start_new_thread(self.ProcessCommands, ())
        self.Logger.info("Processing thread started")

    def ProcessCommands(self):
        while True:
            while len(self.Commands) > 0:
                command = self.Commands.pop(0)
                if command['command'] == "insert":
                    self.Insert(command['data'])
                elif command['command'] == "update":
                    self.Update(command['data'])
                elif command['command'] == "delete":
                    self.Delete(command['data'])
            time.sleep(self.Sleep)
