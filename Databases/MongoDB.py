from Database import Database
from pymongo import MongoClient
import json


class MongoDB(Database):

    def __init__(self, config):
        super(MongoDB, self).__init__(config)
        self.DefineVariables()
        self.SetupMongoClient()

    def SetupMongoClient(self):
        self.Client = MongoClient(self.Address, self.Port)
        self.Database = self.Client[self.DatabaseName]
        self.Collection = self.Database[self.CollectionName]

    def DefineVariables(self):
        try:
            self.Address = self.Config['Address']
            self.Port = int(self.Config['Port']) if "Port" in self.Config else 27017
            self.CollectionName = self.Config['Collection'] if "Collection" in self.Config else "PyNMS"
            self.DatabaseName = self.Config['Database'] if "Database" in self.Config else "PyNMS"
            self.Logger.debug("MongoDB will try to connect to '%s:%i'" % (self.Address, self.Port))
            self.Logger.debug("Using collection '%s'" % self.CollectionName)
        except KeyError as error:
            self.Logger.error("%s not set in config, now exiting!" % error.args[0])
            exit(1)

    def Insert(self, data):
        inserted_id = self.Collection.insert_one(data).inserted_id
        self.Logger.debug("Inserted document with ID %s" % inserted_id)

    def Delete(self, data):
        delete_count = self.Collection.delete_many(data).deleted_count
        self.Logger.info("Deleted %i documents matching '%s'" % (delete_count, json.dumps(data)))

    def Update(self, data):
        result = self.Collection.update_many(data[0], {'$set': data[1]})
        self.Logger.debug("Matched and updated %i documents" % result.modified_count)
