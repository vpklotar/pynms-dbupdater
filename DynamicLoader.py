# This class handles all dynamic loading of all classes
import GlobalConfig


class DynamicLoader:

    def __init__(self):
        self.Logger = GlobalConfig.GetLogger('Dynamic Loader')
        self.Logger.info("Dynamic Loader loaded")

    # Load a Database
    def LoadDatabase(self, name):
        ref = self.Load("Databases." + name)
        return ref

    # Dynamicly load a module given a path
    # Return: A reference that can be instanciated
    def Load(self, path):
        components = path.split('.')
        self.Logger.debug("Trying to load " + path)
        mod = __import__(path)
        for comp in components[1:]:
            mod = getattr(mod, comp)
        ref = getattr(mod, components[-1])
        self.Logger.debug("Load of '" + path + "' completed")
        return ref
