import paho.mqtt.client as mqtt
import _thread
import json
import GlobalConfig


class Mqtt:
    def on_connect(self, mqttc, obj, flags, rc):
        self.Logger.info("Connected to %s:%s" % (mqttc._host, mqttc._port))

    def on_message(self, mqttc, obj, msg):
        payload = self.PayloadToJson(msg.payload.decode('utf-8'))
        if payload is None:
            return
        if msg.topic == self.InsertTopic:
            self.Database.AddCommand({"command": "insert", "data": payload})
        elif msg.topic == self.UpdateTopic:
            self.Database.AddCommand({"command": "update", "data": payload})
        elif msg.topic == self.DeleteTopic:
            self.Database.AddCommand({"command": "delete", "data": payload})
        else:
            self.Logger.debug("Recived unkown command in channel %s" % msg.topic)

    def PayloadToJson(self, data):
        try:
            return json.loads(data)
        except json.decoder.JSONDecodeError:
            self.Logger.debug("Recived data that could not be parsed into JSON, ignoring")
            return None

    def __init__(self, config, db):
        self.Database = db
        self.Logger = GlobalConfig.GetLogger('MQTT')
        self.Logger.debug("Initializing")
        self.config = config

        self.DefineTopics()
        self.SetupMqtt()
        self.MqttThread = _thread.start_new_thread(self.MqttLoop, ())

    def SetupMqtt(self):
        self.mqttc = mqtt.Client("test1")
        self.mqttc.on_message = self.on_message
        self.mqttc.on_connect = self.on_connect
        self.Logger.debug("Hooks implemented")
        self.Logger.debug("Trying to connect to broker")
        self.mqttc.connect("127.0.0.1")
        self.mqttc.subscribe(self.InsertTopic, 0)
        self.mqttc.subscribe(self.UpdateTopic, 0)
        self.mqttc.subscribe(self.DeleteTopic, 0)

    def DefineTopics(self):
        try:
            self.InsertTopic = self.config['InsertTopic']
            self.UpdateTopic = self.config['UpdateTopic']
            self.DeleteTopic = self.config['DeleteTopic']
        except KeyError as error:
            self.Logger.error("%s missing, now exiting" % error.args[0])
            exit(1)

    def MqttLoop(self):
        self.mqttc.loop_start()
