import GlobalConfig
import json


class Config:

    def __init__(self, path):
        self.Logger = GlobalConfig.GetLogger('CONFiG')
        self.Path = path
        self.Logger.info('Loading config \'%s\', please wait' % path)
        self.ReadFile()
        self.Parse()
        self.Logger.info('Config loaded successfully')

    # Read the config file into the variable self.content
    def ReadFile(self):
        try:
            with open(self.Path) as f:
                self.content = f.read()
        except FileNotFoundError:
            self.Logger.error("Config file not found, exiting")
            exit(1)
        self.Logger.debug('Config file read successfully')

    # Parse the self.content variable into json-data and store it in
    # the self.json variable
    def Parse(self):
        self.Logger.debug('Beginning to parse content to JSON')
        try:
            self.JSON = json.loads(self.content)
        except json.decoder.JSONDecodeError:
            self.Logger.error("Invalid JSON-format, now exiting")
            exit(1)
        self.Logger.debug("Parsed config successfully")
