#!/usr/bin/python3
import argparse
import colorlog
import GlobalConfig
import logging
import time
from Config import Config
from Mqtt import Mqtt
from DynamicLoader import DynamicLoader


class DBUpdater:
    Arguments = None  # All alguments passed and parsed
    Database = None  # The database instance that handles all input and modifications

    def __init__(self):
        self.ParseArgs()
        self.InitLogger()
        self.LoadConfig()
        self.LoadDynamicLoader()
        self.LoadDatabase()
        self.LoadMqtt()

    # Load and initialize config
    def LoadConfig(self):
        self.Config = Config(self.Arguments.config)

    def LoadDynamicLoader(self):
        self.DynamicLoader = DynamicLoader()

    def LoadDatabase(self):
        try:
            config = self.Config.JSON['Database']
            db_type = config['Type']
            self.Logger.debug("Trying to load database of type '%s'" % db_type)
            db_reference = self.DynamicLoader.LoadDatabase(db_type)
            self.Database = db_reference(config)
        except KeyError as error:
            if error.args[0] == "Database":
                self.Logger.error('Missing database config, now exiting')
            else:
                self.Logger.error('\'%s\'-key missing from database config. Now exiting' % error.args[0])
            exit(1)
        except ImportError as error:
            self.Logger.error("A database of type '%s' does not exist, now exiting" % db_type)
            exit(1)

    def LoadMqtt(self):
        try:
            self.Logger.debug("Trying to load mqtt")
            config = self.Config.JSON['Mqtt']
            self.Mqtt = Mqtt(config, self.Database)
        except KeyError as error:
            if error.args[0] == "Mqtt":
                self.Logger.error('Missing mqtt config, now exiting')
            else:
                self.Logger.error('\'%s\'-key missing from mqtt config. Now exiting' % error.args[0])
            exit(1)

    def InitLogger(self):
        GlobalConfig.init()
        GlobalConfig.SetValue('LogLevel', self.Arguments.loglevel)
        handler = colorlog.StreamHandler()
        LoggingLevels = {
            'STATUS': 7,
            'PROPERTY': 8
        }
        GlobalConfig.SetValue('LoggingLevels', LoggingLevels)
        logging.addLevelName(LoggingLevels['STATUS'], 'STATUS')
        logging.addLevelName(LoggingLevels['PROPERTY'], 'PROPERTY')
        handler.setFormatter(colorlog.ColoredFormatter(
            '\033[95m[%(asctime)s]%(log_color)s[%(levelname)s]\033[39m\033[1m[%(name)s]\033[0m: %(message)s',
            log_colors={
                'DEBUG':    'cyan',
                'PROPERTY': 'cyan',
                'STATUS':   'cyan',
                'INFO':     'blue',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'red,bg_white',
            }))
        GlobalConfig.SetValue('LogHandler', handler)

        logger = GlobalConfig.GetLogger('DBUpdater')
        logger.debug('Arguments parsed')
        logger.info('Logger initiated')
        self.Logger = logger

    # Parse arguments given via command line
    def ParseArgs(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-c", "--config", type=str,
                            help="Specify a path to a specific config file",
                            default="config.json")
        parser.add_argument("-l", "--loglevel", type=str, default='STATUS',
                            help="Increase logging (MAX: 5, DEFAULT: 5)")
        self.Arguments = parser.parse_args()


DBUpdater()

# Keep the application running
while True:
    time.sleep(1000)
